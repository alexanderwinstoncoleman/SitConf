<?php
/**
 * The header for our theme. 
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AWC_Starter
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'awc-theme' ); ?></a>

	    <!-- HEADER SECTION -->
    <header class="site-header contain-to-grid header-section">
        <!-- TOPBAR SECTION -->
        <nav class="top-bar important-class" data-topbar>
            <!-- TITLE AREA & LOGO -->
            <div class="site-branding title columns large-4">
            	<?php
			if ( is_front_page() && is_home() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php else : ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php
			endif;

			$description = get_bloginfo( 'description', 'display' );
			if ( $description || is_customize_preview() ) : ?>
				<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
			<?php
			endif; ?>
            </div>
            
            <!-- END TITLE AREA & LOGO -->
            <!-- MENU ITEMS -->
            <section class="top-bar-section large-8">
            	<?php 
				$args = array(
                'menu'            => 'main-menu', 
                'container'       => '', 
                'container_class' => 'false', 
                'container_id'    => '',
                'menu_class'      => 'nav right', 
                'echo'            => true,
                'fallback_cb'     => 'wp_page_menu',
                'before'          => '',
                'after'           => '',
                'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                'depth'           => 0,
      );

			wp_nav_menu( $args ); ?>
                <!-- <ul class="right">
                    <li class="active"><a href="index.php">Home</a></li>
                    <li><a href="blog.php">Blog</a></li>
                    <li><a href="#">About us</a></li>
                    <li><a href="#">Portfolio</a></li>
                    <li class="has-dropdown">
                        <a href="#">Services</a>
                        <ul class="dropdown">
                            <li><a href="#">Service #1</a></li>
                            <li><a href="#">Service #2</a></li>
                            <li><a href="#">Service #3</a></li>
                        </ul>
                    </li>
                    <li><a href="#">Contact</a></li>
                </ul> -->
            </section>
            <!-- END MENU ITEMS -->
        </nav>
        <!-- END TOPBAR SECTION -->
    </header>
    <!-- END HEADER SECTION -->

	<header id="masthead" class="site-header row" role="banner">
		<div class="site-branding">
			
		</div><!-- .site-branding -->

		<nav id="site-navigation" class="main-navigation" role="navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'awc-theme' ); ?></button>
			<?php //wp_nav_menu( array( 'theme_location' => 'primary', 'menu_id' => 'primary-menu' ) ); ?>
		</nav><!-- #site-navigation -->
	</header><!-- #masthead -->

	<div id="content" class="site-content">
