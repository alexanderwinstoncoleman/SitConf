<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package AWC_Starter
 */

?>

<?php

$image_alt = get_post_meta( $attachment->ID, '_wp_attachment_image_alt', true);

if( empty($image_alt)){
	$image_alt = get_the_title() . ' Markham and Toronto commercial seating, Ontario';
}

if ( is_single() ){
	// $thumb = get_the_post_thumbnail( $post_id, 'product_thumb' );
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'product_thumb' );
	$classes = 'row';
	$img = '<div class="product-image columns large-6 medium-6 small-12">';
	$img .= '<img src="' . $thumb[0] . '" alt="' . $image_alt . '"/>';
	$img .= '</div>';
}elseif (is_front_page()) {
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'square' );
	$classes = 'columns large-3 medium-3 small-12';
	$img = '<div class="product-image">';
	$img .= '<a href="' . esc_url( get_permalink() ) . '" rel="bookmark"><img src="' . $thumb[0] . '" alt="' . $image_alt . '"/></a>';
	$img .= '</div>';
} else {
	// $thumb = get_the_post_thumbnail( $post_id, 'square' );
	$thumb = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'square' );
	$classes = ''; // we'll figure this out later
	$img = '<div class="product-image">'; // same here
	$img .= '<img src="' . $thumb[0] . '" alt="' . $image_alt . '"/>';
	$img .= '</div>';
}


?>

<article id="post-<?php the_ID(); ?>" <?php post_class($classes); ?>>

	<?php
	
			if ( is_single() ) {
				echo $img;
				echo '<div class="columns large-6 medium-6 small-12">';
				the_title( '<h1 class="entry-title">', '</h1>' );
				echo '</div>';
			} else {
				echo $img;
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			}
	?>

	<header class="entry-header">
		<?php

		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php awc_theme_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'awc-theme' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );

			// wp_link_pages( array(
			// 	'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'awc-theme' ),
			// 	'after'  => '</div>',
			// ) );
		?>
	</div><!-- .entry-content -->

	<!-- <footer class="entry-footer"> -->
		<?php // awc_theme_entry_footer(); ?>
	<!-- </footer>--> <!-- .entry-footer -->
</article><!-- #post-## -->
