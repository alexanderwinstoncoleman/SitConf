<?php
////////////////////////////
// THEME OPTIONS PAGE
////////////////////////////

function theme_settings_page(){
	?>
	    <div class="wrap row collapse">
		    <h1>Theme Settings</h1>
		    <div class="medium-2 columns">
		    	<ul class="tabs vertical" data-tabs id="example-tabs">
			        <li class="tabs-title is-active"><a href="#panel1v" aria-selected="true">Tab 1</a></li>
			        <li class="tabs-title"><a href="#panel2v">Tab 2</a></li>
		      	</ul>
		     </div>
			<div class="medium-10 columns">
		      	<div class="tabs-content vertical" data-tabs-content="example-tabs">
			        <div class="tabs-panel is-active" id="panel1v">
						<form method="post" action="options.php">
					        <?php
					            settings_fields("section");
					            do_settings_sections("theme-options");      
					            
					        ?>          
					    </form>
			        </div>
			        <div class="tabs-panel" id="panel2v">
			        	<p>BLAH BLAH BLAH</p>
			        </div>
		        </div>
	        </div>
		</div>
		<?php submit_button(); ?>
	<?php
}

function add_theme_menu_item()
{
	$theme = wp_get_theme();
	$theme_name	= $theme->name;
	add_menu_page("Theme Panel", $theme_name . " Options", "manage_options", "theme-panel", "theme_settings_page", null, 99);
// 	add_menu_page("META TITLE", "ADMIN LEFT TITLE", "manage_options", "theme-panel", "theme_settings_page", null, 99);
}

add_action("admin_menu", "add_theme_menu_item");

// 

function display_twitter_element()
{
	?>
    	<input type="text" name="twitter_url" id="twitter_url" value="<?php echo get_option('twitter_url'); ?>" />
    <?php
}

function display_facebook_element()
{
	?>
    	<input type="text" name="facebook_url" id="facebook_url" value="<?php echo get_option('facebook_url'); ?>" />
    <?php
}

function display_test_element()
{
	?>
    	<input type="text" name="test_url" id="test_url" value="<?php echo get_option('test_url'); ?>" />
    <?php
}

function display_theme_panel_fields()
{
	add_settings_section("section", "Social Media", null, "theme-options");
	
	add_settings_field("twitter_url", "Twitter Profile Url", "display_twitter_element", "theme-options", "section");
    add_settings_field("facebook_url", "Facebook Profile Url", "display_facebook_element", "theme-options", "section");
    add_settings_field("test_url", "Test Url", "display_test_element", "theme-options", "section");

    register_setting("section", "twitter_url");
    register_setting("section", "facebook_url");
    register_setting("section", "test_url");
}

add_action("admin_init", "display_theme_panel_fields");

// 

function logo_display()
{
	?>
        <input type="file" name="logo" /> 
        <?php echo get_option('logo'); ?>
   <?php
}

function handle_logo_upload()
{
	if(!empty($_FILES["demo-file"]["tmp_name"]))
	{
		$urls = wp_handle_upload($_FILES["logo"], array('test_form' => FALSE));
	    $temp = $urls["url"];
	    return $temp;   
	}
	  
	return $option;
}

function display_theme_logo_panel_fields()
{
	add_settings_section("section", "Social Media Settings", null, "theme-options");
	
    add_settings_field("logo", "Logo", "logo_display", "theme-options", "section");  

    register_setting("section", "logo", "handle_logo_upload");
}

add_action("admin_init", "display_theme_logo_panel_fields");

function awc_social(){
	$fb = get_theme_mod('facebook_id');
	$tw = get_theme_mod('twitter_id');
	$google = get_theme_mod('google_id');
	//$li = get_theme_mod('linkedin');

	// dump($fb);
	$html .= '<ul class="awc-social">';
	if($fb){
		$html .= '<li class="facebook"><a href="' . $fb . '" target="_blank"><i class="fa fa-facebook"></i></a>';
	}

	if($tw){
		$html .= '<li class="facebook"><a href="' . $fb . '" target="_blank"><i class="fa fa-twitter"></i></a>';
	}

	if($google){
		$html .= '<li class="facebook"><a href="' . $fb . '" target="_blank"><i class="fa fa-google-plus"></i></a>';
	}

	// if($linkedin){
	// 	$html .= '<li class="facebook"><a href="' . $fb . '" target="_blank"><i class="fa fa-facebook"></i></a>';
	// }

	$html .= '</ul>';

	return $html;

}

////////////////////////////
// FONTS
////////////////////////////