<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AWC_Starter
 */

?>
		</div> <!-- /.content-row -->
	</div><!-- #content -->
		<div class="full-wrap footer-wrap">
			<footer id="colophon" class="site-footer row" role="contentinfo">
				<div class="site-info">
					<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'awc-theme' ) ); ?>"><?php printf( esc_html__( 'Proudly powered by %s', 'awc-theme' ), 'WordPress' ); ?></a>
					<span class="sep"> | </span>
					<?php $theme = wp_get_theme(); ?>
					<?php printf( esc_html__( 'Theme: %1$s by %2$s.', '$theme' ), $theme->name, '<a href="http://leftsidedesign.ca" rel="designer">alex coleman</a>' ); ?>
				</div><!-- .site-info -->
			</footer><!-- #colophon -->
		</div> <!-- /.full-wrap -->



	<div class="utility-footer">
		<div class="row">
			<div class="columns large-6 medium-6 small-up-6 small-12">&copy; <?php date('Y'); ?> <?php bloginfo( 'name' ); ?> | All Rights Reserved <?php awc_theme_social_media_icons('small'); ?></div>
			<div class="columns large-6 medium-6 small-up-6 small-12 text-right"><?php wp_nav_menu( array( 'theme_location' => 'utility', 'menu_id' => 'utility-menu' ) ); ?></div>
		</div>
	</div> <!-- /utility-footer -->

	</div> <!-- /scroller -->

	</div> <!-- /mp-pusher -->
	</div><!-- #page -->

<?php wp_footer(); ?>
<script>new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById( 'trigger' ) );</script>
</body>
</html>
