<?php
/**
 * AWC Starter functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package AWC_Starter
 */

if ( ! function_exists( 'awc_theme_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function awc_theme_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on AWC Starter, use a find and replace
	 * to change 'awc-theme' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'awc-theme', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'square', 300, 300, array( 'left', 'top' ) );
	add_image_size( 'product-large', 800, 800, array( 'center', 'center' ) );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'awc-theme' ),
		'secondary' => __( 'Secondary Nav - above header', 'awc-theme' ),
		'utility' => __( 'Utility Nav - below footer', 'awc-theme' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See https://developer.wordpress.org/themes/functionality/post-formats/
	 */
	add_theme_support( 'post-formats', array(
		'aside',
		'image',
		'video',
		'quote',
		'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'awc_theme_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	add_theme_support( 'custom-logo', array(
	'height'      => 90,
	'width'       => 382,
	'flex-height' => true,
	'flex-width'  => true,
	'header-text' => array( 'site-title', 'site-description' ),
	) );
}
endif;
add_action( 'after_setup_theme', 'awc_theme_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function awc_theme_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'awc_theme_content_width', 640 );
}
add_action( 'after_setup_theme', 'awc_theme_content_width', 0 );


/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function awc_theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'awc-theme' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'awc_theme_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function awc_theme_scripts() {
	wp_enqueue_style( 'awc-theme-style', get_stylesheet_uri() );

	wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Open+Sans:300italic,400,600,700|Roboto:400,300' );

	wp_enqueue_script( 'theme-scripts', get_template_directory_uri() . '/js/main.min.js', array(), '20151215', true );

	wp_enqueue_style( 'awc-theme-dashicons', get_stylesheet_uri(), 'dashicons' );

	wp_enqueue_style( 'awc-theme-fontawesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css' );

	// wp_enqueue_script( 'awc-theme-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	// wp_enqueue_script( 'awc-theme-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/js/modernizr.custom.js', array(), '', true );
	wp_enqueue_script( 'classie', get_template_directory_uri() . '/js/classie.js', array(), '', true );
	wp_enqueue_script( 'mlpushmenu', get_template_directory_uri() . '/js/mlpushmenu.js', array(), '', true );
	wp_enqueue_script( 'foundation', get_template_directory_uri() . '/js/foundation.js', array(), 'jquery', true );
	wp_enqueue_script( 'foundation-equalizer', get_template_directory_uri() . '/js/foundation.equalizer.js', array(), 'jquery', true );

	if (is_page_template( 'page-locations.php' )) {
		wp_enqueue_script( 'google-maps', 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false');
	}

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'awc_theme_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/* dump(); - makes for easy debugging. <?php dump($post); ?> */
function dump($obj) {
	echo "<pre>";
	print_r($obj);
	echo "</pre>";
}


// SOCIAL Friendly
function do_graphtags() { 

    global $post;
// this first bit is to grab the first image
      $first_img = '';
	  ob_start();
	  ob_end_clean();
	  // $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
	  // $first_img = $matches[1][0];
	  //dump($matches);

    if ( is_page() ) {
        ?>
        <meta property="og:title" content="<?php echo get_bloginfo('title'); ?> | <?php echo the_title(); ?>" />
        <meta property="og:locale" content="en_US" />
        <meta property="og:description" content="<?php echo get_bloginfo( 'description', 'display' ); ?>" />
        <meta property="og:url" content="<?php 
        if(is_front_page() || is_home()) {
        echo get_bloginfo('home').'/';
        } else {
        the_permalink();
        } ?>" />
        <meta property="og:image" content="link/to/your/image.jpg" />
        <link rel="image_src" href="link/to/your/image.jpg" />
        <?php 
    } elseif ( is_single() ) {
            if(has_post_thumbnail($post->ID)) {
                    $img = wp_get_attachment_image_src(get_post_thumbnail_id( $post->ID ), 'medium');
                    $img_src = $img[0];
                } elseif($first_img) {
                		$output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
	  					$first_img = $matches[1][0];
                		$img_src = $first_img;

                } else {
                    $img_src = "link/to/your/image.jpg";
                } // fallback image

                // getting an excerpt or reasonable fascimile
                if(is_single()){
                	if(has_excerpt($post->ID)){
                		$excerpt = get_the_excerpt();
	                } else {
	                	$content = $post->post_content;
	                	$excerpt = wp_trim_words( $content, 15, null );
	                }

                } elseif ( is_page() ) {
                		$excerpt = '<h1>is_page</h1>';
	                	$excerpt = wp_trim_words( $content, 15, null );
                } elseif( is_front_page() ){
                	$excerpt = '<h1>is_front_page</h1>';
                } else{
                	$excerpt = get_bloginfo('description'); 
                }

                ?>
         
            <meta property="og:title" content="<?php echo the_title(); ?>"/>
            <meta property="og:description" content="<?php echo $excerpt; ?>"/>
            <meta property="og:type" content="article"/>
            <meta property="og:url" content="<?php echo the_permalink(); ?>"/>
            <meta property="og:site_name" content="<?php echo get_bloginfo(); ?>"/>
            <meta property="og:image" content="<?php echo $img_src; ?>"/>
         
        <?php
        } else {
            return;
        }


} 
add_action('wp_head', 'do_graphtags'); // end do_graphtags


/**
 * Removes width and height attributes from image tags
 *
 * @param string $html
 *
 * @return string
 */
// function remove_image_size_attributes( $html ) {
//     return preg_replace( '/(width|height)="\d*"/', '', $html );
// }
 
// // Remove image size attributes from post thumbnails
// add_filter( 'post_thumbnail_html', 'remove_image_size_attributes' );
 
// // Remove image size attributes from images added to a WordPress post
// add_filter( 'image_send_to_editor', 'remove_image_size_attributes' );

/* 
*takes user input from the customizer and outputs linked social media icons
*/
function awc_theme_social_media_icons($size) {

    $social_sites = awc_theme_social_media_array();

    if($size == 'large'){
    	$size_class = ' fa-2x';
    } elseif ($size == 'small') {
    	$size_class = '';
    } else{
    	$size_class = '';
    }

    /* any inputs that aren't empty are stored in $active_sites array */
    foreach($social_sites as $social_site) {
        if( strlen( get_theme_mod( $social_site ) ) > 0 ) {
            $active_sites[] = $social_site;
        }
    }

    /* for each active social site, add it as a list item */
        if ( ! empty( $active_sites ) ) {

            echo "<ul class='social-media-icons'>";

            foreach ( $active_sites as $active_site ) {

            	$class = 'fa fa-' . $active_site;

            	switch ($active_site) {
            		case 'facebook': ?>
            			<li>
	                        <a class="<?php echo $active_site; ?>" target="_blank" href="<?php echo esc_url( get_theme_mod( $active_site) ); ?>">
	                            <i class="<?php echo esc_attr( $class ); ?>-official<?php echo $size_class; ?>" title="<?php printf( __('%s icon', 'text-domain'), $active_site ); ?>"></i>
	                        </a>
	                    </li>
                    <?php
            			break;

            		case 'twitter': ?>
            			<li>
	                        <a class="<?php echo $active_site; ?>" target="_blank" href="<?php echo esc_url( get_theme_mod( $active_site) ); ?>">
	                            <i class="<?php echo esc_attr( $class ); ?>-square<?php echo $size_class; ?>" title="<?php printf( __('%s icon', 'text-domain'), $active_site ); ?>"></i>
	                        </a>
	                    </li>
                    <?php
            			break;

            		case 'google-plus': ?>
            			<li>
	                        <a class="<?php echo $active_site; ?>" target="_blank" href="<?php echo esc_url( get_theme_mod( $active_site) ); ?>">
	                            <i class="<?php echo esc_attr( $class ); ?>-square<?php echo $size_class; ?>" title="<?php printf( __('%s icon', 'text-domain'), $active_site ); ?>"></i>
	                        </a>
	                    </li>
                    <?php
            			break;
            		
            		default: ?>
            			<li>
	                        <a class="<?php echo $active_site; ?>" target="_blank" href="<?php echo esc_url( get_theme_mod( $active_site) ); ?>">
	                            <i class="<?php echo esc_attr( $class ); ?><?php echo $size_class; ?>" title="<?php printf( __('%s icon', 'text-domain'), $active_site ); ?>"></i>
	                        </a>
                    	</li>
            			<?php
            			break;
            	}

            }
            echo "</ul>";
        }
}


////////////////////////////
// WALKER
////////////////////////////

class Sitconf_Walker extends Walker_Nav_Menu
{
    function start_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<div class='sub-menu-wrap mp-level'><a class='mp-back' href='#''><i class='fa fa-arrow-circle-o-left fa-2x' aria-hidden='true'></i></a><ul class='sub-menu'>\n";
    }
    function end_lvl( &$output, $depth = 0, $args = array() ) {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul></div>\n";
    }
}

////////////////////////////
// PRODUCT FEATURED IMAGE CHANGE
////////////////////////////

add_action('do_meta_boxes', 'replace_featured_image_box');  
function replace_featured_image_box()  
{  
    remove_meta_box( 'postimagediv', 'product', 'side' );  
    add_meta_box('postimagediv', __('Main Product Image'), 'post_thumbnail_meta_box', 'product', 'side', 'low');
}