<?php
/**
 * Template Name: Wood Finishes
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package AWC_Starter
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<div class="row">

			<?php
			$args = array(
				'post_type' => 'finish',
				'posts_per_page' => -1,
				'order' => 'ASC',
				);
			$q = new WP_Query( $args );
			while ( $q->have_posts() ) : $q->the_post();

				get_template_part( 'partials/content', 'finish' );

			endwhile; // End of the loop.
			?>
			</div>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
