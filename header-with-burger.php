<?php
/**
 * The header for our theme. 
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package AWC_Starter
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<?php do_graphtags(); ?>

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site container">
	<div class="mp-pusher" id="mp-pusher">
	<a href="#" id="trigger" class="menu-trigger show-for-small-only">Menu</a>
	<!-- <a href="#" id="trigger" class="menu-trigger nav-icon2 show-for-small-only">
	  <span></span>
	  <span></span>
	  <span></span>
	  <span></span>
	  <span></span>
	  <span></span>
	</a> -->
		<header id="masthead" class="site-header row" role="banner">
			<div class="site-branding columns large-4 medium-4 small-12 hide-for-small-only">
			<?php 
				if ( function_exists( 'the_custom_logo' ) ) {
					the_custom_logo();
				}
			 ?>
				<h1 class="site-title hide-for-small-only"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
				$description = get_bloginfo( 'description', 'display' );
				if ( $description || is_customize_preview() ) : ?>
					<p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
				<?php
				endif; ?>
			</div><!-- .site-branding -->

			<div class="columns large-8 medium-8 small-12 text-right align-self-bottom hide-for-small-only">
				<div class="social-search clear row no-margin-left no-margin-right float-right">

					<?php awc_theme_social_media_icons('large'); ?>
					<?php get_search_form(); ?>
					
				</div>
				<nav id="mp-menu" class="main-navigation mp-menu" role="navigation">

					<?php 
						wp_nav_menu(
						    array (
						        'theme_location' => 'primary',
						        'container_class' => 'mp-level',
						        'container_id' => '',
						        'walker'         => new Suite_22_Walker,
						    )
						); 
					?>
				</nav><!-- #site-navigation -->
			</div>
		</header>
	<div class="scroller">
	<div id="content" class="site-content">

		<div class="content-row row scroller-inner">
		
