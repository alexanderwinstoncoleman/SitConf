<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package AWC_Starter
 */

?>

<?php 

if(!is_single()){
	$classes = 'columns large-3 medium-6 small-12';
} else{
	$classes = '';
}

?>

<article id="post-<?php the_ID(); ?>" <?php post_class($classes); ?>>
	<header class="entry-header">
		<?php
			if ( is_single() ) {
				the_title( '<h1 class="entry-title">', '</h1>' );
			} else {
				the_title( '<h2 class="entry-title">', '</h2>' );
			}
			echo '<p class="finish-code">' . wood_finish_code_get_meta( 'wood_finish_code_code' ) . '</p>';
			?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
		if(has_post_thumbnail( $post_id )){ ?>
			<div class="finish-thumb">
			<?php the_post_thumbnail(); ?>
			</div>
		<?php }
		
		?>
	</div><!-- .entry-content -->
</article><!-- #post-## -->