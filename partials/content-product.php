<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package AWC_Starter
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php
			if ( is_single() ) {
				the_title( '<h1 class="entry-title">', '</h1>' );
			} else {
				the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
			}

			$back = get_field( 'back' );
			$seat_height = get_field( 'seat_height' );
			$depth = get_field( 'seat_depth' );
			$seat_width = get_field( 'seat_width' );

			$width_height = ' width="20" height="32"';

			// dump($back);
			if ($back || $seat_height || $depth || $seat_width) {
				echo '<div class="dimensions">';
				echo '<h3>Dimensions</h3>';
			

				if ($back) {
					# code...
					echo '<img src="' . get_template_directory_uri() . '/dimensions/chair-dimensions-back-small.png"' . $width_height . ' />';
					echo $back . "\" high";
				}

				if ($seat_height) {
					# code...
					echo '<img src="' . get_template_directory_uri() . '/dimensions/chair-dimensions-leg-small.png"' . $width_height . ' />';
					echo $seat_height . "\" seat height";
				}

				if ($depth) {
					# code...
					echo '<img src="' . get_template_directory_uri() . '/dimensions/chair-dimensions-seat-small.png"' . $width_height . ' />';
					echo $depth . "\" seat depth";
				}

				if ($seat_width) {
					# code...
					echo '<img src="' . get_template_directory_uri() . '/dimensions/chair-dimensions-seat-width-small.png"' . $width_height . ' />';
					echo $seat_width . "\" seat width";
				}

				echo '</div>';

			}
		?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
			the_content( sprintf(
				/* translators: %s: Name of current post. */
				wp_kses( __( 'Continue reading %s <span class="meta-nav">&rarr;</span>', 'awc-theme' ), array( 'span' => array( 'class' => array() ) ) ),
				the_title( '<span class="screen-reader-text">"', '"</span>', false )
			) );
		?>
	</div><!-- .entry-content -->
<?php //get_template_part( 'partials/related-posts' ); ?>

<?php $orig_post = $post;
global $post;
$categories = get_the_category($post->ID);
if ($categories) {

$category_ids = array();
foreach($categories as $individual_category) $category_ids[] = $individual_category->term_id;

$args=array(
'category__in' => $category_ids,
'post__not_in' => array($post->ID),
'posts_per_page'=> 5, // Number of related posts that will be displayed.
'caller_get_posts'=>1,
'orderby'=>'rand' // Randomize the posts
);
$my_query = new wp_query( $args );
if( $my_query->have_posts() ) {
	echo 'aldsfjalskdjhlakshdlkjahsdlkfhad';
echo '<div id="perfect-related_by-category" class="clear"><h3>Related Posts</h3><ul>';
while( $my_query->have_posts() ) {
$my_query->the_post(); ?>
<li>
 <?php the_title(); ?>
</li>
<?php }
echo '</ul></div>';
} }
$post = $orig_post;
wp_reset_query(); ?>

	<footer class="entry-footer">
		<?php awc_theme_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->