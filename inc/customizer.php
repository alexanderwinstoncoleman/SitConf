<?php
/**
 * AWC Starter Theme Customizer.
 *
 * @package AWC_Starter
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */

function awc_theme_social_media_array() {

	/* store social site names in array */
	$social_sites = array('instagram', 'facebook', 'twitter', 'pinterest', 'google-plus');

	return $social_sites;
}

function awc_theme_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

		// SOCIAL MEDIA



	$wp_customize->add_section( 'social_links', array(
		'priority'		=> 100,
		'capability'	=> 'edit_theme_options',
		'title'			=> __( 'Social Links', 'textdomain' ),
	));

	$social_sites = awc_theme_social_media_array();
		$priority = 5;

		foreach($social_sites as $social_site) {

			$wp_customize->add_setting( "$social_site", array(
					'type'              => 'theme_mod',
					'capability'        => 'edit_theme_options',
					'sanitize_callback' => 'esc_url_raw'
			) );

			$wp_customize->add_control( $social_site, array(
					'label'    => __( ucwords($social_site), 'text-domain' ),
					'section'  => 'social_links',
					'type'     => 'url',
					'priority' => $priority,
					'input_attrs' => array(
					    'placeholder' => __( 'http://' . $social_site . '.com/...' ),
					  ),
			) );

			$priority = $priority + 5;
		}

		// CONTACT

	$wp_customize->add_section( 'contact_info', array(
		'priority'		=> 100,
		'capability'	=> 'edit_theme_options',
		'title'			=> __( 'Contact Info', 'textdomain' ),
	));

	$wp_customize->add_setting( 'email', array(
		'default'	=> '',
		'type'		=> 'theme_mod',
		'capability'	=> 'edit_theme_options',
		'transport'		=> '',
		//'sanitize_callback'	=> 'esc_url',
	));

	$wp_customize->add_control( 'email', array(
		'type' => 'url',
	    'priority' => 10,
	    'section' => 'contact_info',
	    'label' => __( 'Email', 'textdomain' ),
	    'description' => '',
	));

	$wp_customize->add_setting( 'phone', array(
		'default'	=> '',
		'type'		=> 'theme_mod',
		'capability'	=> 'edit_theme_options',
		'transport'		=> '',
		//'sanitize_callback'	=> 'esc_url',
	));

	$wp_customize->add_control( 'phone', array(
		'type' => 'text',
	    'priority' => 10,
	    'section' => 'contact_info',
	    'label' => __( 'Phone (you need the country code; use this format: 14164164164)', 'textdomain' ),
	    'description' => '',
	));

	$wp_customize->add_setting( 'name', array(
		'default'	=> '',
		'type'		=> 'theme_mod',
		'capability'	=> 'edit_theme_options',
		'transport'		=> '',
		//'sanitize_callback'	=> 'esc_url',
	));

	$wp_customize->add_control( 'name', array(
		'type' => 'text',
	    'priority' => 10,
	    'section' => 'contact_info',
	    'label' => __( 'Contact Name', 'textdomain' ),
	    'description' => '',
		'input_attrs' => array(
		    'placeholder' => __( 'John Doe / Big Widget Inc.' ),
		  ),
	));

	$wp_customize->add_setting( 'city', array(
		'default'	=> '',
		'type'		=> 'theme_mod',
		'capability'	=> 'edit_theme_options',
		'transport'		=> '',
		//'sanitize_callback'	=> 'esc_url',
	));

	$wp_customize->add_control( 'city', array(
		'type' => 'text',
	    'priority' => 10,
	    'section' => 'contact_info',
	    'label' => __( 'City', 'textdomain' ),
	    'description' => '',
		'input_attrs' => array(
		    'placeholder' => __( 'Big Fat City' ),
		  ),
	));

	$wp_customize->add_setting( 'address', array(
		'default'	=> '',
		'type'		=> 'theme_mod',
		'capability'	=> 'edit_theme_options',
		'transport'		=> '',
		//'sanitize_callback'	=> 'esc_url',
	));

	$wp_customize->add_control( 'address', array(
		'type' => 'text',
	    'priority' => 10,
	    'section' => 'contact_info',
	    'label' => __( 'Street Address', 'textdomain' ),
	    'description' => '',
		'input_attrs' => array(
		    'placeholder' => __( '123 Main St.' ),
		  ),
	));

	$wp_customize->add_setting( 'postal', array(
		'default'	=> '',
		'type'		=> 'theme_mod',
		'capability'	=> 'edit_theme_options',
		'transport'		=> '',
		//'sanitize_callback'	=> 'esc_url',
	));

	$wp_customize->add_control( 'postal', array(
		'type' => 'text',
	    'priority' => 10,
	    'section' => 'contact_info',
	    'label' => __( 'Postal Code, Zip, etc...', 'textdomain' ),
	    'description' => '',
		'input_attrs' => array(
		    'placeholder' => __( 'M6G 6M6' ),
		  ),
	));

}
add_action( 'customize_register', 'awc_theme_customize_register' );

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function awc_theme_customize_preview_js() {
	wp_enqueue_script( 'awc_theme_customizer', get_template_directory_uri() . '/js/customizer.js', array( 'customize-preview' ), '20151215', true );
}
add_action( 'customize_preview_init', 'awc_theme_customize_preview_js' );
