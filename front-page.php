<?php
/**
 * The template for the front page.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package AWC_Starter
 */

get_header(); ?>

	<div id="primary" class="content-area columns large-12 medium-12 small-12">
		<main id="main" class="site-main" role="main">

		<div class="clear carousel js-flickity" data-flickity-options='{ "imagesLoaded": true }'>
		<?php
			$args = array(
				'post_type' => 'slider',
				'posts_per_page' => 4,
				'orderby' => 'rand',
				);
			$loop = new WP_Query( $args );
			if ( $loop->have_posts() ) {
				while ( $loop->have_posts() ) : $loop->the_post(); 

				$img = get_field('image');

				?>

				<div class="carousel-cell">
					<?php
						echo '<img src="' . $img['url'] . '" alt="' . $img['alt'] . '" width="' . $img['width'] . '" height="' . $img['height'] . '"/>'
					?>
				</div>
					
				<?php endwhile;
			}
			wp_reset_postdata();
		?>
		 
		</div> <!-- /flickity -->

		<div class="clear products large-up-4 medium-up-4 small-up-2 small-12">
	<?php
		$args = array(
			'post_type' => 'product',
			'posts_per_page' => 8,
			'orderby' => 'rand',
			);
		$loop = new WP_Query( $args );
		if ( $loop->have_posts() ) {
			while ( $loop->have_posts() ) : $loop->the_post();
				get_template_part( 'template-parts/content', 'product' );
			endwhile;
		} else {
			echo __( 'No products found' );
		}
		wp_reset_postdata();
	?>
</div><!--/.products-->


			<?php
				/**
				 * The WordPress Query class.
				 * @link http://codex.wordpress.org/Function_Reference/WP_Query
				 *
				 */
				$args = array(
					
					//Type & Status Parameters
					'post_type'   => 'collection',
					
					//Pagination Parameters
					'posts_per_page'         => 1,
					
					
					
				);
			
			$q = new WP_Query( $args );

			if ($q->have_posts()) :
				echo '<div class="row large-up-5 medium-up-4 small-up-2">';
					while ($q->have_posts()) : $q->the_post(); 

					// the_title();
					// the_post_thumbnail();
					// echo get_the_date();
			
			// check if the repeater field has rows of data
			if( have_rows('info') ):

			 	// loop through the rows of data
			    while ( have_rows('info') ) : the_row();

			        // display a sub field value
			        $name = get_sub_field('name');
			        $image = get_sub_field('logo');
			        $url = get_sub_field('url');
			        echo '<div class="column">';
			        echo '<a href="' . $url . '" target="_blank" title="' . $name . '">';
			        echo '<img src="' . $image['sizes']['medium'] . '" alt="' . $image['alt'] . '" width="' . $image['sizes']['medium-width'] . '" height="' . $image['sizes']['medium-height'] . '" />';
			        echo '</a>';
			        echo '</div>';
			    endwhile;

			else :

			    // no rows found

			endif;
			
			endwhile;
				echo '</div>';
			endif;
			?>



		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
